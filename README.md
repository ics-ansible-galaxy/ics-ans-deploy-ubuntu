# ics-ans-deploy-ubuntu

Ansible playbook to deploy a bootable ubuntu/debian system using a live centos.

## License

BSD 2-clause

```
ubuntu_version: bionic
ubuntu_repo: http://archive.ubuntu.com/ubuntu/
ubuntu_chroot_path: "/mnt/chroot"
ubuntu_fake_fstab: "/tmp/ubuntu_fake_fstab"
ubuntu_csi_password: "alessio"  # CHANGEME
ubuntu_deploy_user: csi
ubuntu_rootfs_label: rootfs
ubuntu_md_device: md42
ubuntu_dns_search_list:
  - cslab.esss.lu.se
ubuntu_dns_servers_list:
  - 8.8.8.8
  - 1.1.1.1
ubuntu_netplan_config:
  network:
    ethernets:
      deploy:
        match:
          macaddress: "{{ ansible_default_ipv4.macaddress }}"
        addresses:
          - "{{ (ansible_default_ipv4.address + '/' + ansible_default_ipv4.netmask) | ipaddr('host/prefix') }}"
        gateway4: "{{ ansible_default_ipv4.gateway }}"
        nameservers:
          search: "{{ ubuntu_dns_search_list }}"
          addresses: "{{ ubuntu_dns_servers_list }}"
    version: 2
```

To define the target disk, it's possible to provide a list or the targets dimensions.
If `ubuntu_target_disks` is defines, the autodetecting by the disk size is not run.
```
ubuntu_target_auto_size: "15.00 GB"
```
Or
```
ubuntu_target_disks:
 - /dev/sdb
```
