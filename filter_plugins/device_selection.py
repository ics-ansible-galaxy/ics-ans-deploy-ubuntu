import string


class FilterModule(object):
    def filters(self):
        return {
            'devices_autoselect_by_size': self.devices_autoselect_by_size,
            'devices_autoselect_by_model': self.devices_autoselect_by_model,
            'devices_umount_detect': self.devices_umount_detect,
            'devices_swraid_detect': self.devices_swraid_detect,
            'devices_and_partitons': self.devices_and_partitons,
            'partitions_template': self.partitions_template,
            'partitions_template_list': self.partitions_template_list,
            'get_devices_details': self.get_devices_details}

    def get_devices_details(self, ansible_devices):
        ret = []
        for device_key, device_values in ansible_devices.items():
            ret.append([device_key, device_values['size'], device_values['model']])
        return ret

    def devices_autoselect_by_size(self, ansible_devices, size):
        ret = []
        for device_key, device_values in ansible_devices.items():
            if device_values['size'] == size:
                ret.append("/dev/{}".format(device_key))
        return ret

    def devices_autoselect_by_model(self, ansible_devices, model):
        ret = []
        for device_key, device_values in ansible_devices.items():
            if device_values['model'] == model:
                ret.append("/dev/{}".format(device_key))
        return ret

    def devices_and_partitons(self, ansible_devices, selected_devices):
        ret = []
        for device in selected_devices:
            ret.append(device)
            for partition in ansible_devices[device.split('/')[-1]]['partitions']:
                ret.append("/dev/{}".format(partition))
        return sorted(ret, reverse=True)

    def devices_umount_detect(self, ansible_mounts, selected_devices):
        ret = []
        for mount in ansible_mounts:
            if mount['device'].rstrip(string.digits) in selected_devices or mount['device'].startswith('md'):
                ret.append(mount['mount'])
        return ret

    def devices_swraid_detect(self, ansible_devices, selected_devices):
        ret = []
        for device in selected_devices:
            for partition_k, partition_v in ansible_devices[device.split('/')[-1]]['partitions'].items():
                # part = ansible_devices[device]['partitions'][partition]
                for master in partition_v['links']['masters']:
                    if master.startswith('md'):
                        ret.append(master)
            for master in ansible_devices[device.split('/')[-1]]['links']['masters']:
                if master.startswith('md'):
                    ret.append(master)
        return ret

    def partitions_template(self, partition):
        """Add a 'p' at the end of the partition the nvme string is found."""
        if not isinstance(partition, str):
            partition = partition[0]
        if 'nvme' in partition:
            return f"{partition}p"
        return partition

    def partitions_template_list(self, partitions):
        ret = []
        for partition in partitions:
            ret.append(self.partitions_template(partition))
        return ret
